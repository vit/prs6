class CreateLibraryItemFiles < ActiveRecord::Migration
  def change
    create_table :library_item_files do |t|
      t.string :sid
      t.string :aasm_state
      t.integer :position, default: 0
      t.string :file_type
      t.references :item, index: true, foreign_key: true
      t.string :file

      t.timestamps null: false
    end
    add_index :library_item_files, :sid, unique: true
  end
end

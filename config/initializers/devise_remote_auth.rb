
#=begin

require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class ExternalAuthenticatable < Authenticatable
#    class ExternalAuthenticatable < DatabaseAuthenticatable
      def authenticate!
        if params[:user]
#          user = User.enter_by_pin userpin, password
          user = User.enter_by_pin pin, password
          if user
            success!(user)
          else
            fail(:invalid_login)
          end
        end
      end

      def email
        params[:user][:email]
      end

      def pin
        params[:user][:pin].to_i
      end
#      def userpin
#        params[:user][:userpin].to_i
#      end

      def password
        params[:user][:password]
      end

    end
  end
end

Warden::Strategies.add(:external_authenticatable, Devise::Strategies::ExternalAuthenticatable)


require 'devise/strategies/database_authenticatable'

module Devise
  module Models
    module ExternalAuthenticatable
      extend ActiveSupport::Concern
    end
  end
end



Devise.with_options model: true do |d|
  # Strategies first
  d.with_options strategy: true do |s|
    routes = [nil, :new, :destroy]
    s.add_module :external_authenticatable, controller: :sessions, route: { session: routes }
#    s.add_module :rememberable, no_input: true
  end

  # Other authentications
#  d.add_module :omniauthable, controller: :omniauth_callbacks,  route: :omniauth_callback

  # Misc after
#  routes = [nil, :new, :edit]
#  d.add_module :recoverable,  controller: :passwords,     route: { password: routes }
#  d.add_module :registerable, controller: :registrations, route: { registration: (routes << :cancel) }
#  d.add_module :validatable

  # The ones which can sign out after
#  routes = [nil, :new]
#  d.add_module :confirmable,  controller: :confirmations, route: { confirmation: routes }
#  d.add_module :lockable,     controller: :unlocks,       route: { unlock: routes }
#  d.add_module :timeoutable

  # Stats for last, so we make sure the user is really signed in
#  d.add_module :trackable
end






=begin

module Devise
  module Models
    module ExternalAuthenticatable
      extend ActiveSupport::Concern

      #
      # Here you do the request to the external webservice
      #
      # If the authentication is successful you should return
      # a resource instance
      #
      # If the authentication fails you should return false
      #
      def remote_authentication(authentication_hash)
        # Your logic to authenticate with the external webservice
      end

      module ClassMethods
        ####################################
        # Overriden methods from Devise::Models::Authenticatable
        ####################################

        #
        # This method is called from:
        # Warden::SessionSerializer in devise
        #
        # It takes as many params as elements had the array
        # returned in serialize_into_session
        #
        # Recreates a resource from session data
        #
        def serialize_from_session(id)
          resource = self.new
          resource.id = id
          resource
        end

        #
        # Here you have to return and array with the data of your resource
        # that you want to serialize into the session
        #
        # You might want to include some authentication data
        #
        def serialize_into_session(record)
          [record.id]
        end

      end
    end
  end
end

=end

=begin
module Devise
  module Models
    module RemoteAuthenticatable
      extend ActiveSupport::Concern

      #
      # Here you do the request to the external webservice
      #
      # If the authentication is successful you should return
      # a resource instance
      #
      # If the authentication fails you should return false
      #
      def remote_authentication(authentication_hash)
        # Your logic to authenticate with the external webservice
      end

      module ClassMethods
        ####################################
        # Overriden methods from Devise::Models::Authenticatable
        ####################################

        #
        # This method is called from:
        # Warden::SessionSerializer in devise
        #
        # It takes as many params as elements had the array
        # returned in serialize_into_session
        #
        # Recreates a resource from session data
        #
        def serialize_from_session(id)
          resource = self.new
          resource.id = id
          resource
        end

        #
        # Here you have to return and array with the data of your resource
        # that you want to serialize into the session
        #
        # You might want to include some authentication data
        #
        def serialize_into_session(record)
          [record.id]
        end

      end
    end
  end
end



module Devise
    module Strategies
      class RemoteAuthenticatable < Authenticatable
        #
        # For an example check : https://github.com/plataformatec/devise/blob/master/lib/devise/strategies/database_authenticatable.rb
        #
        # Method called by warden to authenticate a resource.
        #
        def authenticate!
          #
          # authentication_hash doesn't include the password
          #
          auth_params = authentication_hash
          auth_params[:password] = password

          #
          # mapping.to is a wrapper over the resource model
          #
          resource = mapping.to.new

          return fail! unless resource

          # remote_authentication method is defined in Devise::Models::RemoteAuthenticatable
          #
          # validate is a method defined in Devise::Strategies::Authenticatable. It takes
          #a block which must return a boolean value.
          #
          # If the block returns true the resource will be loged in
          # If the block returns false the authentication will fail!
          #
          if validate(resource){ resource.remote_authentication(auth_params) }
            success!(resource)
          end
        end
      end
    end
  end

=end





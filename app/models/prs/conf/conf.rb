
class Prs::Conf::Conf < Prs::Base
	CONF_CLASS = 'COMS:CONF'

	def self.get_conf_data _id
		db[:conf].find( {'_meta.class' => CONF_CLASS, _id: _id} ).first
	end
	def self.get_conf_info _id
		res = get_conf_data _id
		res.is_a?(Hash) ? res['info'] : nil
	end

end

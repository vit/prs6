class CreateLibraryItems < ActiveRecord::Migration
  def change
    create_table :library_items do |t|
      t.string :sid
      t.string :parent
      t.string :aasm_state
      t.integer :position, default: 0
      t.string :item_type
      t.string :title
      t.string :subtitle
      t.text :abstract
      t.integer :children_seq, default: 0

      t.timestamps null: false
    end
    add_index :library_items, :sid, unique: true
  end
end

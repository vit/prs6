class RenameColumnParentToParentIdForLibraryItem < ActiveRecord::Migration
  def change
  	rename_column :library_items, :parent, :parent_id
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151029142513) do

  create_table "library_item_files", force: :cascade do |t|
    t.string   "sid"
    t.string   "aasm_state"
    t.integer  "position",   default: 0
    t.string   "file_type"
    t.integer  "item_id"
    t.string   "file"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "library_item_files", ["item_id"], name: "index_library_item_files_on_item_id"
  add_index "library_item_files", ["sid"], name: "index_library_item_files_on_sid", unique: true

  create_table "library_items", force: :cascade do |t|
    t.string   "sid"
    t.string   "parent_id"
    t.string   "aasm_state"
    t.integer  "position",     default: 0
    t.string   "item_type"
    t.string   "title"
    t.string   "subtitle"
    t.text     "abstract"
    t.integer  "children_seq", default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.boolean  "is_dir"
  end

  add_index "library_items", ["sid"], name: "index_library_items_on_sid", unique: true

  create_table "users", force: :cascade do |t|
    t.string   "sid"
    t.string   "mid"
    t.string   "aasm_state"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "userpin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["mid"], name: "index_users_on_mid", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["sid"], name: "index_users_on_sid", unique: true
  add_index "users", ["userpin"], name: "index_users_on_userpin", unique: true

end

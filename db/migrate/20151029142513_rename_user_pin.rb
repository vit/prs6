class RenameUserPin < ActiveRecord::Migration
  def change
  	rename_column :users, :pin, :userpin
  end
end

class AddIsDirToLibaryItem < ActiveRecord::Migration
  def change
	add_column :library_items, :is_dir, :boolean
  end
end

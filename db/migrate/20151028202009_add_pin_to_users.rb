class AddPinToUsers < ActiveRecord::Migration
  def change
    add_column :users, :pin, :integer
    add_index :users, :pin, unique: true
  end
end

require 'elasticsearch/model'


class Library::Item < ActiveRecord::Base
	include AASM

  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks


	def as_indexed_json(options={})
#		as_json(only: [:id, :sid, :title, :subtitle, :abstract])
		rez = as_json(
#			methods: [:to_text_test], only: [:id, :sid, :title, :subtitle, :abstract, :to_text_test],
#			only: [:id, :sid, :title, :subtitle, :abstract],
#			include: {
#				item_files: { methods: [:to_text], only: [:to_text] }
#			}
			methods: [:files_to_text], only: [:id, :sid, :title, :subtitle, :abstract, :files_to_text]
#			only: [:id, :sid, :title, :subtitle, :abstract, :to_text_test]
#			only: [:id, :sid, :title, :subtitle, :abstract, :to_text_test], methods: [:to_text_test]
#			only: [:id, :sid, :title, :subtitle, :abstract],
#			include: {
#				item_files: { methods: [:to_text], only: [:to_text] }
#			}
		)
#		puts rez
		rez
	end

#	def to_text_test
#		["rqtqrtqwreqtq", "abrakadabra"]
#	end
	def files_to_text
		item_files.map(&:to_text)
	end

	has_many :item_files, dependent: :destroy
	 #, class_name: "Library::ItemFile", foreign_key: "parent_id", primary_key: "sid", dependent: :destroy
	has_many :children, class_name: "Library::Item", foreign_key: "parent_id", primary_key: "sid", dependent: :destroy
 	belongs_to :parent, class_name: "Library::Item", primary_key: "sid"

#	PTYPES = [:conference, :journal]
	ITEMTYPES = [:folder, :conference, :journal, :conference_paper, :journal_paper]

	validates_uniqueness_of :sid

	aasm do
		state :just_created, initial: true
		state :draft
		state :published
		state :nonexistent
		event :sm_init do
			after do
				#create_new_revision
			end
			#transitions :from => :just_created, :to => :draft
			transitions :from => :just_created, :to => :published
		end
	end


	def self.new_object data
		library_item = Library::Item.new(data)
		if library_item.save
			library_item.sm_init!
			library_item
		else
			nil
		end
	end





	# From Old Mongo

	def self.get_confs_list
		db[:conf].find( {'_meta.class' => "COMS:CONF"} ).sort( [[ '_meta.ctime', -1]] ).inject([]) do |acc,c|
			acc << {'_id' => c['_id'], '_meta' => c['_meta'], 'info' => c['info']}
		end.select{ |c| c['info'] && (c['info']['status']=='active' || c['info']['status']=='archived') }
	end

=begin
	#!!!
	def self.get_paper_files_list cont_id, _id, cl=nil
		@collfiles.find( cl ?
			{'_meta.class' => check_file_class(cl), '_meta.parent' => _id, '_meta.cont_id' => cont_id} :
			{'_meta.parent' => _id, '_meta.cont_id' => cont_id}
		).map do |f|
			get_file_info_from_file f
		end
	end

	#!!!
	def self.check_file_class cn
		case cn.to_s.to_sym
		when :abstract then CONF_PAPER_ABSTRACT_FILE_CLASS
		when :abstract_exdoc then CONF_PAPER_ABSTRACT_EXDOC_FILE_CLASS
		when :paper then CONF_PAPER_PAPER_FILE_CLASS
		when :paper_exdoc then CONF_PAPER_PAPER_EXDOC_FILE_CLASS
		when :presentation then CONF_PAPER_PRESENTATION_FILE_CLASS
	#	when :exdoc then CONF_PAPER_EXDOC_FILE_CLASS
		else 'INEXISTENT'
		end
	end
=end

#	def import_children_from_conference parent, conf_id, papers
	def self.import_children_from_conference parent_item, papers, lang='ru'
		rez = []
		papers.each do |p_id|
			p = Prs::Conf::Paper.get_paper_info p_id
			data = {
				is_dir: false,
				parent: parent_item,
				item_type: :conference_paper,
				title: p['text']['title'][lang],
#				subtitle: p['authors'] ? p['authors'].map{ |a| {fname: a['fname'][lang], mname: a['mname'][lang], lname: a['lname'][lang] } } : [],
				subtitle: p['authors'] ?
					p['authors'].map{ |a| a['fname'][lang] + ' ' + a['mname'][lang] +' '+ a['lname'][lang] }.join(', ') :
					[],
				abstract: p['text']['abstract'][lang]
			}
			item = new_object data

			if p['files']
				p['files'].each do |f|
					if f[:class_code]=='paper' && lang==f[:_meta]['lang']
						rez << item.sid
						fdata = Prs::Conf::Paper.get_paper_file p_id, lang, 'paper'
				        if fdata
							src = StringIO.new(fdata.data)
							class << src; attr_accessor :original_filename; end
							src.original_filename = fdata.filename
				        	f = item.item_files.create(file: src, file_type: :paper)
				        	item.save!
				        end
					end
				end
			end
		end
		rez
	end
	def self.import_whole_conference parent_item, conf_id, lang='ru'
		c = Prs::Conf::Conf.get_conf_info conf_id
#		puts "!!!!!!!!!!!!!!"
#		puts c
			data = {
				is_dir: true,
				parent: parent_item,
				item_type: :conference,
				title: c['title'][lang],
				subtitle: '',
				abstract: c['description'][lang]
			}
			item = new_object data
			list = Prs::Conf::Paper.get_conf_papers_list conf_id
			papers = list.map(){|p| p['_id']}
#			papers = list.map(:_id)
			import_children_from_conference item, papers, lang

	end

	def self.db
		Mongoid::Clients.default
	end


end


#Library::Item.import


class CreateUsers < ActiveRecord::Migration
  def change
    drop_table :users
    
    create_table :users do |t|
      t.string :sid
      t.string :mid
      t.string :aasm_state

      t.timestamps null: false
    end
    add_index :users, :sid, unique: true
    add_index :users, :mid, unique: true
  end
end

#class << Mongo::Grid::File::Info; attr_reader :_meta; end
class << Mongo::Grid::File::Info; attr_reader :document; end


class Prs::Conf::Paper < Prs::Base

	CONF_PAPER_CLASS = 'COMS:CONF:PAPER'
	CONF_PAPER_ABSTRACT_FILE_CLASS = 'COMS:CONF:PAPER:ABSTRACT:FILE'
	CONF_PAPER_ABSTRACT_EXDOC_FILE_CLASS = 'COMS:CONF:PAPER:ABSTRACT_EXDOC:FILE'
	CONF_PAPER_PAPER_FILE_CLASS = 'COMS:CONF:PAPER:PAPER:FILE'
	CONF_PAPER_PAPER_EXDOC_FILE_CLASS = 'COMS:CONF:PAPER:PAPER_EXDOC:FILE'
	CONF_PAPER_PRESENTATION_FILE_CLASS = 'COMS:CONF:PAPER:PRESENTATION:FILE'
	CONF_PAPER_EXDOC_FILE_CLASS = 'COMS:CONF:PAPER:EXDOC:FILE'

	def self.check_file_class cn
		case cn.to_s.to_sym
		when :abstract then CONF_PAPER_ABSTRACT_FILE_CLASS
		when :abstract_exdoc then CONF_PAPER_ABSTRACT_EXDOC_FILE_CLASS
		when :paper then CONF_PAPER_PAPER_FILE_CLASS
		when :paper_exdoc then CONF_PAPER_PAPER_EXDOC_FILE_CLASS
		when :presentation then CONF_PAPER_PRESENTATION_FILE_CLASS
	#	when :exdoc then CONF_PAPER_EXDOC_FILE_CLASS
		else 'INEXISTENT'
		end
	end
	def self.file_class_short_code cl
		case cl
	#	when CONF_PAPER_ABSTRACT_FILE_CLASS then 'a'
	#	when CONF_PAPER_PAPER_FILE_CLASS then 'p'
	#	when CONF_PAPER_PRESENTATION_FILE_CLASS then 'pr'
		when CONF_PAPER_ABSTRACT_FILE_CLASS then 'abstract'
		when CONF_PAPER_ABSTRACT_EXDOC_FILE_CLASS then 'abstract_exdoc'
		when CONF_PAPER_PAPER_FILE_CLASS then 'paper'
		when CONF_PAPER_PAPER_EXDOC_FILE_CLASS then 'paper_exdoc'
		when CONF_PAPER_PRESENTATION_FILE_CLASS then 'presentation'
	#	when CONF_PAPER_EXDOC_FILE_CLASS then 'exdoc'
		else 'inexistent'
		end
	end

#	def get_paper_info cont_id, _id
	def self.get_paper_info _id
		c = db[:'conf.paper'].find(
			{'_meta.class' => CONF_PAPER_CLASS, '_id' => _id}
		).first
		{'_id' => c['_id'], '_meta' => c['_meta'],
			'text' => c['text'],
			'keywords' => c['keywords'],
			'authors' => c['authors'],
			'files' => get_paper_files_list(c['_id'])
		}
	end

	def self.get_conf_papers_list cont_id
		db[:'conf.paper'].find(
#			{'_meta.class' => 'COMS:CONF:PAPER', '_meta.parent' => cont_id}
			{'_meta.class' => CONF_PAPER_CLASS, '_meta.parent' => cont_id}
		).sort( [[ '_meta.ctime', -1]] ).inject([]) do |acc,c|
			acc << {'_id' => c['_id'], '_meta' => c['_meta'],
				'text' => c['text'],
				'keywords' => c['keywords'],
				'authors' => c['authors'],
			#	'files' => get_paper_abstract_files_list(cont_id, c['_id'])
				'files' => get_paper_files_list(c['_id'])
			}
		end
	end



	def self.get_paper_files_list _id, cl=nil
		db[:'conf.paper.files'].find( cl ?
			{'_meta.class' => check_file_class(cl), '_meta.parent' => _id} :
			{'_meta.parent' => _id}
		).map do |f|
			get_file_info_from_file f
		end
	end
	def self.get_file_info_from_file f
		f ? {
			_id: f['_id'],
    		content_type: f['contentType'],
			length: f['length'],
			filename: f['filename'],
		#	uniquefilename: f['_meta']['parent']+'_a_'+f['_meta']['lang']+'_'+f['filename'],
			uniquefilename: f['_meta']['parent']+'_'+file_class_short_code(f['_meta']['class'])+'_'+f['_meta']['lang']+'_'+f['filename'],
			class_code: file_class_short_code(f['_meta']['class']),
			_meta: f['_meta']
		} : nil
	end
	def self.find_paper_file _id, lang, cl
#		client.database.fs(:fs_name => 'grid')
		grid = db.database.fs(:fs_name => 'conf.paper')
		res = grid.find_one('_meta.class' => check_file_class(cl), '_meta.parent' => _id, '_meta.lang' => lang)
#		puts ">>>>> FILE"
#		puts res.info.inspect
#		puts res.info.filename.inspect
#		puts res.info.document.inspect
#		puts "<<<<< FILE"
#		res ? res['_id'] : nil
		res
	end
	def self.get_paper_file _id, lang, cl
#		fid = find_paper_file _id, lang, cl
#		fid ? @grid.get(fid) : nil
		find_paper_file _id, lang, cl
	end

end

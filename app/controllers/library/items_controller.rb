class Library::ItemsController < ApplicationController
  before_action :set_library_item, only: [:show, :edit, :update, :destroy, :file_upload, :file_delete]
  before_action :set_parent_item

  # GET /library/items
  # GET /library/items.json
  def index
    #@library_items = Library::Item.all
    @library_item = nil
#    show
    get_items
    @confs_list = Library::Item.get_confs_list
    @can_edit = current_user && current_user.is_admin
    render :show
  end

  # GET /library/items/1
  # GET /library/items/1.json
  def show
    get_items @library_item
    @library_item_files = @library_item ? @library_item.item_files : []
    @library_item_file = @library_item ? Library::ItemFile.new( {item: @library_item} ) : nil

    @confs_list = Library::Item.get_confs_list

    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
#    @can_edit = true
  end

  def search
    @search_query = params['q']
#    response = Library::Item.search @search_query
#    response = Library::Item.search query: { match:  { title: @search_query } }
    response = Library::Item.search({query:
          {
            bool: {
              should: [
                {
                  multi_match: {
                    query: @search_query,
#                    fields: ['title^3', 'subtitle^2', 'abstract'],
#                    fields: ['title^3', 'subtitle^2', 'abstract', 'item_files.to_text', 'to_text_test'],
                    fields: ['title^10', 'subtitle^7', 'abstract^5', 'files_to_text'],
                    operator: 'and',
                    #analyzer: 'russian_morphology_custom'
                  }
                }
              ]
            }
          }
        }, size: 100)

#    @search_records = response.records
    @library_items = response.records
#    puts records
  end


  # GET /library/items/new
  def new
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    unless @can_edit
      redirect_to @parent_item ? @parent_item : library_root_path
      return
    end
  #    @library_item = Library::Item.new
  #    @library_item = Library::Item.new( @parent_item ? {parent: @parent_item.sid} : {} )
      @library_item = Library::Item.new( @parent_item ? {parent: @parent_item} : {} )
  end

  # GET /library/items/1/edit
  def edit
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    unless @can_edit
      redirect_to @library_item
      return
    end
  end

  def file_upload
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    if @can_edit
      @library_item_file = @library_item ? Library::ItemFile.new( {item: @library_item} ) : nil
      data = params[:library_item_file]
      if data
        file_type = data[:file_type] || 'paper'
        file = data[:file]
        if file
          f = @library_item.item_files.create(file: file, file_type: file_type)
          f.save!
          @library_item.save!
        end
      end
    end

    @library_item_files = @library_item ? @library_item.item_files : []
    respond_to do |format|
      format.js { }
#      format.html { }
#      format.json { }
    end

  end

  def file_delete
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    if @can_edit
      item_file_id = params[:item_file]
      item_file = @library_item.item_files.where({sid: item_file_id}).first
      item_file.destroy
    end

    @library_item_files = @library_item ? @library_item.item_files : []
    respond_to do |format|
        format.js { }
#        format.html { }
#        format.json { }
    end
  end


  # POST /library/items
  # POST /library/items.json
  def create
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    respond_to do |format|
      unless @can_edit
        format.html { redirect_to library_items_url, notice: "You can't create new item" }
        format.json { head :no_content }
      else
        @library_item = Library::Item.new(library_item_params.merge({}))
        if @library_item.save
          @library_item.sm_init!
          format.html { redirect_to @library_item, notice: 'Item was successfully created.' }
          format.json { render :show, status: :created, location: @library_item }
        else
          format.html { render :new }
          format.json { render json: @library_item.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /library/items/1
  # PATCH/PUT /library/items/1.json
  def update
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    respond_to do |format|
      unless @can_edit
        format.html { redirect_to @library_item, notice: "You can't change it" }
        format.json { head :no_content }
      else
        if @library_item.update(library_item_params)
          format.html { redirect_to @library_item, notice: 'Item was successfully updated.' }
          format.json { render :show, status: :ok, location: @library_item }
        else
          format.html { render :edit }
          format.json { render json: @library_item.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /library/items/1
  # DELETE /library/items/1.json
  def destroy
    @can_edit = current_user && current_user.is_admin
#    @can_edit = false
    parent = @library_item.parent
    respond_to do |format|
      unless @can_edit
        format.html { redirect_to @library_item, notice: "You can't delete it" }
        format.json { head :no_content }
      else
        @library_item.destroy
#       format.html { redirect_to library_items_url, notice: 'Item was successfully destroyed.' }
        format.html { redirect_to parent || library_items_url, notice: 'Item was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end



  def conferences
    respond_to do |format|
      format.js {}
    end
  end

  def conf_papers
    @conf_id = params['conf_id']
    @library_item_id = params['library_item']
#    @papers_list = Library::Item.get_conf_papers_list @conf_id
    @papers_list = Prs::Conf::Paper.get_conf_papers_list @conf_id
    respond_to do |format|
      format.js {}
    end
  end

  def import_conf_papers
    @library_item_id = params['library_item']
    @library_item = Library::Item.find(@library_item_id)
    papers = params['papers']
    conf_id = params['conf_id']
    lang = params['lang']
    if papers
      Library::Item.import_children_from_conference @library_item, papers, lang
    elsif conf_id
      Library::Item.import_whole_conference @library_item, conf_id, lang
    end
    get_items @library_item
    respond_to do |format|
      format.js {}
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_library_item
      @library_item = Library::Item.find(params[:id])
      current = @library_item
      @library_item_ancestors = []
      while current do
        @library_item_ancestors.unshift current
        current = current.parent
      end
    end
    def set_parent_item
      @parent_item = nil
      parent_id = params[:parent]
#      puts "!!!!!!!!!!!!!!!!!!!!!!"
#      puts parent_id
      if parent_id
        @parent_item = Library::Item.find(parent_id)
      end
#      puts @parent_item
#      puts params
#      puts "!!!!!!!!!!!!!!!!!!!!!!"
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def library_item_params
#      params.require(:library_item).permit(:sid, :parent, :aasm_state, :position, :is_dir, :item_type, :title, :subtitle, :abstract, :children_seq)
#      params.require(:library_item).permit(:is_dir, :parent, :item_type, :title, :subtitle, :abstract)
      params.require(:library_item).permit(:is_dir, :parent_id, :item_type, :title, :subtitle, :abstract)
    end

    def get_items parent=nil
      @library_items = parent.nil? ? Library::Item.where({parent: nil}) : parent.children.all
#      @library_items = parent.nil? ? Library::Item.all : parent.children.all
#      @library_items = parent.nil? ? Library::Item.all : parent.is_dir ? parent.children.all : []
#      @library_items = Library::Item.all
    end

end

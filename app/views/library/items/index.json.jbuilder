json.array!(@library_items) do |library_item|
  json.extract! library_item, :id, :sid, :parent, :aasm_state, :position, :item_type, :title, :subtitle, :abstract, :children_seq
  json.url library_item_url(library_item, format: :json)
end

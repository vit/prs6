class Library::ItemFile < ActiveRecord::Base
	include AASM

  belongs_to :item
  mount_uploader :file, ::LibraryItemFileUploader

	FILETYPES = [:paper, :abstract, :presentation]

	aasm do
		state :just_created, initial: true
	end

	def to_text
#		t = Yomu.new(file.current_path).text rescue ""
#		t
		""
	end

end

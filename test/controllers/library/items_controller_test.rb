require 'test_helper'

class Library::ItemsControllerTest < ActionController::TestCase
  setup do
    @library_item = library_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:library_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create library_item" do
    assert_difference('Library::Item.count') do
      post :create, library_item: { aasm_state: @library_item.aasm_state, abstract: @library_item.abstract, children_seq: @library_item.children_seq, item_type: @library_item.item_type, parent: @library_item.parent, position: @library_item.position, sid: @library_item.sid, subtitle: @library_item.subtitle, title: @library_item.title }
    end

    assert_redirected_to library_item_path(assigns(:library_item))
  end

  test "should show library_item" do
    get :show, id: @library_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @library_item
    assert_response :success
  end

  test "should update library_item" do
    patch :update, id: @library_item, library_item: { aasm_state: @library_item.aasm_state, abstract: @library_item.abstract, children_seq: @library_item.children_seq, item_type: @library_item.item_type, parent: @library_item.parent, position: @library_item.position, sid: @library_item.sid, subtitle: @library_item.subtitle, title: @library_item.title }
    assert_redirected_to library_item_path(assigns(:library_item))
  end

  test "should destroy library_item" do
    assert_difference('Library::Item.count', -1) do
      delete :destroy, id: @library_item
    end

    assert_redirected_to library_items_path
  end
end

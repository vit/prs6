json.extract! @library_item, :id, :sid, :parent, :aasm_state, :position, :item_type, :title, :subtitle, :abstract, :children_seq, :created_at, :updated_at
